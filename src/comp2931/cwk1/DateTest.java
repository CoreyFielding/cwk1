package comp2931.cwk1;

import org.junit.Before;
import org.junit.Test;

import java.util.GregorianCalendar;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class DateTest {
    private Date date1;
    private Date date2;

    @Before
    public void setUp() {
        date1 = new Date(2017, 10, 30);
        date2 = new Date(1997, 07, 28);
    }

    @Test
    public void testToSring() {
        assertThat(date1.toString(), is("2017-10-30"));
        assertThat(date1.toString().equals("17-10-30"), is(false));

        assertThat(date2.toString(), is("1997-07-28"));
        assertThat(date2.toString().equals("97-7-28"), is(false));
    }

    @Test(expected = IllegalArgumentException.class)
    public void yearTooLow() {
        new Date(-1, 0, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void monthTooLow() {
        new Date(0, -1, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void monthTooHigh() {
        new Date(0, 13, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void dayTooLow() {
        new Date(0, 0, -1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void dayTooHigh() {
        new Date(0, 0, 32);
    }

    @Test(expected = IllegalArgumentException.class)
    public void febDayTooHigh() {
        new Date(0, 02, 29);
    }

    @Test(expected = IllegalArgumentException.class)
    public void aprDayTooHigh() {
        new Date(0 , 04, 31);
    }

    @Test(expected = IllegalArgumentException.class)
    public void novDayTooHigh() {
        new Date(0, 11, 31);
    }

    @Test
    public void testEquals() {
        assertTrue(date1.equals(date1));
        assertFalse(date1.equals(new Date(1, 1, 1)));
        assertFalse(date1.equals(new Date(2017, 12, 31)));

        assertTrue(date2.equals(date2));
        assertFalse(date2.equals(new Date(1, 1, 1)));
        assertFalse(date2.equals(new Date(1997, 07, 29)));
    }

    @Test
    public void testDayOfYear() {
        assertThat(new Date(2017, 1, 1), is(1));
        assertThat(new Date(1997, 12, 31), is(365));
    }
}