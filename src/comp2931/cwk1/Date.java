// Class for COMP2931 Coursework 1

package comp2931.cwk1;

import java.util.GregorianCalendar;

/**
 * Simple representation of a date.
 */
public class Date {

  private int year;
  private int month;
  private int day;

  /**
   * Creates a date using the given values for year, month and day.
   *
   * @param y Year
   * @param m Month
   * @param d Day
   */
  public Date(int y, int m, int d) {
    year = y;
    month = m;
    day = d;

    /**
     * Ensure a valid data is always generated and throw exception if boundaries are exceeded
     */

    if (year < 1) {
      throw new IllegalArgumentException("Invalid year");
    }

    if (month == 13 || month < 01) {
      throw new IllegalArgumentException("Invalid month");
    }

    if (month == 02 && day > 28) {
      throw new IllegalArgumentException("Day too high");
    }

    if (month == 11 && day > 30) {
      throw new IllegalArgumentException("Day too high");
    }

    if (month == 04 && day > 30) {
      throw new IllegalArgumentException("Day too high");
    }

    if (day > 32 || day < 01) {
      throw new IllegalArgumentException("Invalid day given");
    }
  }

  /**
   * Returns the year component of this date.
   *
   * @return Year
   */
  public int getYear() {
    return year;
  }

  /**
   * Returns the month component of this date.
   *
   * @return Month
   */
  public int getMonth() {
    return month;
  }

  /**
   * Returns the day component of this date.
   *
   * @return Day
   */
  public int getDay() {
    return day;
  }

  /**
   * Provides a string representation of this date.
   * <p>
   * ISO 8601 format is used (YYYY-MM-DD).
   *
   * @return Date as a string
   */
  @Override
  public String toString() {
    return String.format("%04d-%02d-%02d", year, month, day);
  }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Date date = (Date) o;

        if (year != date.year) return false;
        if (month != date.month) return false;
        return day == date.day;
    }

    @Override
    public int hashCode() {
        int result = year;
        result = 31 * result + month;
        result = 31 * result + day;
        return result;
    }

    /**
   * Compares dates for equality of state
   * Returns true
   *
   * @return true
   */



  public int getDayOfYear() {
      GregorianCalendar gregCal = new GregorianCalendar();
      gregCal.set(GregorianCalendar.YEAR, getYear());
      gregCal.set(GregorianCalendar.MONTH, getMonth());
      gregCal.set(GregorianCalendar.DATE, getDay());

      int DAY_OF_YEAR = gregCal.get(GregorianCalendar.DATE);
      return DAY_OF_YEAR;
  }
}